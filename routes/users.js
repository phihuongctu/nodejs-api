var express = require("express");
var router = express.Router();

router.get("/", function (req, res, next) {
  res.render("users/list", {
    cn: 'danh sach'
  });
});
router.get("/add", function (req, res) {
  res.render("users/add", {
    cn: "add"
  });
});
router.get("/delete", function (req, res) {
  res.render("users/delete", {
    cn: "delete"
  });
});
router.get("/edit", function (req, res) {
  res.render("users/edit", {
    cn: "edit"
  });
});
module.exports = router;